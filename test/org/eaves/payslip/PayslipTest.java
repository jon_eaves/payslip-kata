package org.eaves.payslip;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PayslipTest {
    
    // TODO: purpose of these new tests are to provide a safe framework to extract out the Tax calculations
    // and unit test them thoroughly.  The tests for that will be found in the TaxScheduleTests
    
    @Test
    public void canCreatePayslipBasedOnNoTax() {
        Employee e = new Employee("jon", "eaves", 60000, 10);
        Payslip p = e.pay("06 March", "06 April", new AlwaysZeroTaxationSchedule());
        
        assertEquals(0, p.tax());
    }
    
    
}
