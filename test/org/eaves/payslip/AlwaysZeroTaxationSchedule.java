package org.eaves.payslip;

public class AlwaysZeroTaxationSchedule implements TaxationSchedule {

    @Override
    public int tax(int salary) {
        return 0;
    }

}
