package org.eaves.payslip;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AppTest {
    
    @Test
    public void createPayslipForEmployee() {
        Employee johndoe = new Employee("John", "Doe", 60050, 9);
        Payslip march = johndoe.pay("1 March", "31 March", new AustralianTaxationSchedule());
        
        assertEquals("John Doe", march.displayName());
        assertEquals("01 March - 31 March", march.displayPayPeriod());
        assertEquals(5004, march.grossIncome());
        assertEquals(450, march.superannuation());

        assertEquals(922, march.tax());
        assertEquals(4082, march.netIncome());
    }
    
    @Test
    public void createPayslipForDifferentEmployee() {
        Employee janedoe = new Employee("Jane", "Doe", 12000, 5);
        Payslip september = janedoe.pay("1 September", "31 September", new AustralianTaxationSchedule());

        assertEquals("Jane Doe", september.displayName());
        assertEquals("01 September - 30 September", september.displayPayPeriod());
        assertEquals(1000, september.grossIncome());
        assertEquals(50, september.superannuation());
        
        assertEquals(0, september.tax());
        assertEquals(1000, september.netIncome());
        
    }
    
}
