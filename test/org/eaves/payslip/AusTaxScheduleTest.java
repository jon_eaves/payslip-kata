package org.eaves.payslip;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AusTaxScheduleTest {

    @Test
    public void testOriginalRequirement() {
        TaxationSchedule t = new AustralianTaxationSchedule();

        assertEquals(922, t.tax(60050));
    }
    
    private int calculations[][] = {
            {18200, 0},
            {18250, 1},
            {37000, 298},
            {37001, 298},
            {36999, 298},
            {180000, 4519},
            {180020, 4520},
            {0, 0},
            {1, 0},
            {87000,1652},
            {87001,1652}
    };
    

    @Test
    public void calculationTestAgainstComputedMatrix() {
        TaxationSchedule t = new AustralianTaxationSchedule();

        for ( int[] v : calculations) {
            int salary = v[0];
            int monthlyTax = v[1];
            
            assertEquals(monthlyTax, t.tax(salary));
                    
        }
    }
}
    
