package org.eaves.payslip;

public class Employee {
    private final String _displayName;
    private final int _superValue;
    private final int _salaryInDollars;

    public Employee(String firstName, String lastName, int salaryInDollars, int superPercentage) {
        _displayName = formatDisplayName(firstName, lastName);
        _salaryInDollars = salaryInDollars;
        _superValue = monthlySuper(salaryInDollars, superPercentage);
    }
    
    private int monthlySalary(int salaryInDollars) {
        return rounded(salaryInDollars / 12);
    }
    
    private int monthlySuper(int salaryInDollars, int superPercentage) {
        return rounded( (monthlySalary(salaryInDollars) * superPercentage / 100));
    }
    
    
    private int rounded(double value) {
        return (int) Math.round(value);
    }

    private String formatDisplayName(String firstName, String lastName) {
        return firstName + " " + lastName;
    }

    public Payslip pay(String startDate, String endDate, TaxationSchedule taxSchedule) {
        int taxViaSchedule = taxSchedule.tax(_salaryInDollars);
        int monthlySalary = monthlySalary(_salaryInDollars);
        return new Payslip(_displayName, PayDate.dateConvert(startDate), PayDate.dateConvert(endDate), 
                monthlySalary, _superValue, taxViaSchedule, monthlySalary - taxViaSchedule);
    }

}
