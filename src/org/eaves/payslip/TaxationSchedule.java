package org.eaves.payslip;

public interface TaxationSchedule {
    
    int tax(int salary);
    
}
