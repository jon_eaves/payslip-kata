package org.eaves.payslip;

public class Payslip {
    
    private final String _displayName;
    private final String _displayPeriod;
    private final int _grossIncome;
    private final int _superValue;
    private final int _monthlyTax;
    private final int _netIncome;

    public Payslip(String displayName, String startOfPeriod, String endOfPeriod, 
                   int grossIncome, int superValue, int monthlyTax, int netIncome) {
        _displayName = displayName;
        _displayPeriod = startOfPeriod + " - " + endOfPeriod;
        _grossIncome = grossIncome;
        _superValue = superValue;
        _monthlyTax = monthlyTax;
        _netIncome = netIncome;
    }

    public String displayName() {
        return _displayName;
    }

    public String displayPayPeriod() {
        return _displayPeriod;
    }

    public int grossIncome() {
        return _grossIncome;
    }

    public int tax() {
        return _monthlyTax;
    }

    public int netIncome() {
        return _netIncome;
    }

    public int superannuation() {
        return _superValue;
    }
}
