package org.eaves.payslip;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class PayDate {

    private static final DateTimeFormatter IN_FORMAT = DateTimeFormatter.ofPattern("d MMMM uuuu", Locale.ENGLISH);
    private static final DateTimeFormatter OUT_FORMAT = DateTimeFormatter.ofPattern("dd MMMM", Locale.ENGLISH);

    static String dateConvert(String date) {
        return LocalDate.parse(date+" "+ LocalDate.now().getYear(), IN_FORMAT).format(OUT_FORMAT);
    }
}
