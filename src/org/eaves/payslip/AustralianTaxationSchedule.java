package org.eaves.payslip;

public class AustralianTaxationSchedule implements TaxationSchedule {

    @Override
    public int tax(int salaryInDollars) {
        double bracketTax = 0;
        double rateInTenthCents = 0;
        double residual = salaryInDollars;

        if (salaryInDollars > 180000) {
            bracketTax = 54232;
            rateInTenthCents = 450;
            residual = salaryInDollars - 180000;
        } else if (salaryInDollars > 87000) {
            bracketTax = 19822;
            rateInTenthCents = 370;
            residual = salaryInDollars - 87000;
        } else if (salaryInDollars > 37000) {
            bracketTax = 3572;
            rateInTenthCents = 325;
            residual = salaryInDollars - 37000;
        } else if (salaryInDollars > 18200) {
            bracketTax = 0;
            rateInTenthCents = 190;
            residual = salaryInDollars - 18200;
        }

        double yearlyTax = (bracketTax + ((residual * rateInTenthCents) / 1000.00));

        return (int) Math.round(yearlyTax / 12);        
    }

}
